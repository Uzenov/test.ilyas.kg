<?php

namespace App\Services;

use App\Helpers\StorageHelper;
use App\Models\CommonFile;
use App\Repositories\StorageFileRepository;
use App\Repositories\StorageServiceRepository;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Support\Facades\DB;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StorageService
{

    const UPLOAD_DIR = 'uploads';

    private $serviceRepository;
    private $fileRepository;

    public function __construct()
    {
        $this->serviceRepository = new StorageServiceRepository;
        $this->fileRepository = new StorageFileRepository;
    }

    public function relativeFileName(int $serviceId, string $fileName): string
    {
        $path = $this->relativePathByServiceId($serviceId);
        $baseFileName = basename($fileName);
        return $path . '/' . $baseFileName;
    }

    public function fileUrl(int $serviceId, string $fileName)
    {
        $relativeFileName = $this->relativeFileName($serviceId, $fileName);
        return url($relativeFileName);
    }

    private function isExistsFileInDb(string $modelName, int $modelId): bool
    {
        $connection = DB::connection();
        $builder = $connection->table('common_files');
        $builder->where('model', '=', $modelName);
        $builder->where('model_id', '=', $modelId);
        $count = $builder->count();
        return $count > 0;
    }

    private function insertFileInDb(string $modelName, int $modelId, string $fileName) {
        $commonFile = new CommonFile;
        $commonFile->path = basename($fileName);
        $commonFile->model = $modelName;
        $commonFile->model_id = $modelId;
        $commonFile->save();
    }

    private function isExistsFileInFs(int $serviceId, string $fileName): bool
    {
        $targetDir = $this->pathByServiceId($serviceId);
        $fullFileName = $targetDir . DIRECTORY_SEPARATOR . basename($fileName);
        return file_exists($fullFileName);
    }

    public function attach(int $serviceId, string $fileName, int $modelId) {
        $service = $this->serviceRepository->oneServiceById($serviceId);
        $isExistsFileInFs = $this->isExistsFileInFs($serviceId, $fileName);
        if( ! $isExistsFileInFs) {
            throw new Exception('File not exists');
        }
        $isExistsFileInDb = $this->isExistsFileInDb($service['name'], $modelId);
        if( ! $isExistsFileInDb) {
            $this->insertFileInDb($service['name'], $modelId, $fileName);
        }
    }

    public function saveFile(int $serviceId, UploadedFile $file): string
    {
        $targetDir = $this->pathByServiceId($serviceId);
        $targetFileName = $this->fileName($file);
        $file->move($targetDir, $targetFileName);
        $fileName = $targetDir . DIRECTORY_SEPARATOR . $targetFileName;
        $fileName = $this->fileRepository->compressImage($fileName);
        return $fileName;
    }

    private function moveToTemp(UploadedFile $file): string
    {
        $targetDir = $this->pathByServiceId(0);
        $targetFileName = $this->fileName($file);
        $file->move($targetDir, $targetFileName);
        return $targetDir . DIRECTORY_SEPARATOR . $targetFileName;
    }

    private function relativePathByServiceId(int $serviceId): string
    {
        $service = $this->serviceRepository->oneServiceById($serviceId);
        $targetDir = self::UPLOAD_DIR . '/' . $service['path'];
        return $targetDir;
    }

    private function fileName(UploadedFile $file): string
    {
        $extension = $file->getClientOriginalExtension();
        $fileName = $file->getPathname();
        $hash = StorageHelper::fileHash($fileName);
        $targetFileName = $hash . '.' . $extension;
        return $targetFileName;
    }

    private function thumbPathByServiceId(int $serviceId, int $size): string
    {
        $targetDir = $this->pathByServiceId($serviceId);
        $path = $targetDir . DIRECTORY_SEPARATOR . 'thumb' . strval($size);
        \File::makeDirectory($path);
        return $path;
    }

    private function pathByServiceId(int $serviceId): string
    {
        $relativePath = $this->relativePathByServiceId($serviceId);
        $targetDir = StorageHelper::path('public/' . $relativePath);
        return StorageHelper::normalizeFilePath($targetDir);
    }

}
