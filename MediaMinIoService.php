<?php


namespace App\Services\Media;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Media;
use phpDocumentor\Reflection\Types\Boolean;

class MediaMinIoService {

    private $storage;

    private $resize = true;

    public function __construct(){
        $this->storage = new StorageMinio();
    }

    /**
     * Загрузка изображения
     * @param UploadedFile $image
     * @param $imageableType
     * @param $imageableId
     * @param array $conversions
     * @return mixed
     * @throws Exception
     */
    public function upload(UploadedFile $image, $imageableType, $imageableId, array $conversions = []) {
        /**
         * Проверяем есть ли в конфиг файле переданные конверсии
         */
        if (count($conversions) && $this->resize) {
            foreach ($conversions as $conversion) {
                if (!in_array($conversion, config('media.conversions'))) {
                    throw new Exception('Conversion ' . $conversion . ' not present in config');
                }
            }
        }

        /**
         * Если изображение не валидно выбросим исклчение
         */
        if (!$image->isValid()) {
            throw new Exception('Argument $image is not valid media resource');
        }

        $clientName = $image->getClientOriginalName();
        $originalFileName = Str::uuid() . '.' . $image->guessClientExtension();

        $media = new Media();

        if($image->guessClientExtension() == "svg") {
            // сохраняю оригинальное изображение
            $this->storage->save($image->path(), $originalFileName);

            return $media->create([
                'imageable_type' => $imageableType,
                'imageable_id' => $imageableId,
                'client_file_name' => $clientName,
                'original_file_name' => $originalFileName,
                'size' => $image->getSize(),
                'mime' => $image->getMimeType()
            ]);
        }


        // создаем инстанс Intervention Image
        $img = Image::make($image);

        // сохраняем оригинальное изображение
        $img->save($image->getPath() . "/" . $originalFileName ,90);

        $size = $img->filesize();
        $mime = $img->mime();
        // сохраняю оригинальное изображение
        $this->storage->save($image->path(), $originalFileName);

        // определяем конверсии. Берем из параметра или из конфига
        $converted = null;

        if ($this->resize) {
            $converted = $this->saveConversion($image, $originalFileName, $conversions);
        }

        return $media->create([
            'imageable_type' => $imageableType,
            'imageable_id' => $imageableId,
            'client_file_name' => $clientName,
            'original_file_name' => $originalFileName,
            'conversions' => $converted,
            'size' => $size,
            'mime' => $mime
        ]);
    }

    private function saveConversion(UploadedFile $file, $originalFileName, array $conversions){
        $actualConversions = (count($conversions)) ? $conversions : config('media.conversions');
        $converted = [];

        foreach ($actualConversions as $conversion => $size) {
            $img = Image::make($file);

            $img->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $convertedName = $conversion . '_' . $originalFileName;

            $fullFileName = $img->dirname . "/" . $convertedName;
            $img->save($fullFileName, 90);

            $this->storage->save($fullFileName, $convertedName);

            $converted[$conversion] = [
                'name' => $convertedName,
                'width' => $img->width(),
                'height' => $img->height(),
                'mime' => $img->mime()
            ];
        }
        return $converted;
    }

    public function setResize($resize = true)
    {
        $this->resize = $resize;
    }


    /**
     * Установка изображения главным
     * @param $mediaId
     * @param $imageableType
     * @param $imageableId
     */
    public function setMainById($mediaId, $imageableType, $imageableId)
    {

        // сначала сделаем уже главное изображение не главным
        // если такое есть
        Media::where('imageable_type', $imageableType)
               ->where('imageable_id', $imageableId)
               ->update(['main_image' => 0]);

        $media = Media::find($mediaId);
        $media->main_image = 1;
        $media->save();
    }

    /**
     * Удаление изображение для сущности
     * @param $imageableType
     * @param $imageableId
     * @throws Exception
     */
    public function deleteForModel($imageableType, $imageableId)
    {
        $images = Media::where('imageable_type', $imageableType)->where('imageable_id', $imageableId)->get();

        foreach ($images as $image) {
            $this->delete($image);
        }
    }

    /**
     * Приватный метод для удаления изображений для модели
     * @param Media $media
     * @throws Exception
     */
    private function delete(Media $media)
    {

        /**
         * Если удаляемое изображение является главным для сущности
         * то найдем первое попавшееся не главное и сделаем его главным
         */
        if ($media->main_image) {
            $mainMedia = Media::where('imageable_type', $media->imageable_type)
                ->where('imageable_id', $media->imageable_id)
                ->where('id', '!=', $media->id)
                ->where('main_image', 0)->first();

            if ($mainMedia) {
                $mainMedia->main_image = 1;
                $mainMedia->save();
            }
        }

        $this->deleteFile($media->getOriginal('original_file_name'));
        $conversions = $media->conversions;

        if(!is_null($conversions)) {
            foreach ($conversions as $k => $conversion) {
                $this->deleteFile($conversion['name']);
            }
        }

        $media->delete();

    }



    /**
     * Удаление файла
     * @param string $name
     */
    public function deleteFile($name)
    {
        $this->storage->delete($name);
    }

    /**
     * Удаление изображения по его айди
     * @param $mediaId
     * @throws Exception
     */
    public function deleteById(int $mediaId)
    {
        $media = Media::find($mediaId);
        $this->storage->delete($media->original_file_name);
    }

    public function getMedia(int $mediaId)
    {
        $media = Media::find($mediaId);
        return $media;
    }

    public function getMediaForType(string $imageableType, int $imageableId)
    {
        return Media::where('imageable_type', $imageableType)->where('imageable_id', $imageableId)->get();
    }
}
