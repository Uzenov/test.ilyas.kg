<?php


namespace App\Services\Media;

use Illuminate\Support\Facades\Storage;

class StorageMinio{
    private $storage;
    const TIME_SHARE = '+5 minutes';

    public function __construct(){
        $this->storage = Storage::disk('minio');
    }

    public function save($fullFileName, $saveAsFileName){
        $fileContent = file_get_contents($fullFileName);
        $this->storage->put($saveAsFileName, $fileContent);
    }

    public function delete($fileName) {
        return $this->storage->delete($fileName);
    }

    public function exists($fileName) {
        return $this->storage->exists($fileName);
    }

    public function getPublicUrl($fileName) {
        return $this->storage->url($fileName);
    }

    public function read($fileName) {
        return $this->storage->get($fileName);
    }

}